<?php
declare(strict_types = 1);

use Core\App;

define('APP_BASE_PATH', dirname(__DIR__));
define('APP_IMG_PATH', APP_BASE_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'img');

require_once '../vendor/autoload.php';

$app = App::getApp();
$app->start();