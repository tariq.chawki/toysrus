# Catalogue de jeux

Ce projet est un catalogue de jeux qu'on à fait en 
PHP Objet avec le model MVC durant notre formation

## Getting started

Se rendre dans la racine du projet avec la command suivant:

```
cd site-toysrus
```

Puis démarrer le projet avec docker:

```
docker compose up --build
```

Ensuite imported le fichier sql 'toys-r-us.sql' dans la base de donnée:

```
// Démarrer le prompt mysql sur docker
// le password: 'root'
docker compose exec db mysql -u root -p 

// Une fois dans le prompt Mysql se rendre sur toysrus
use toysrus;

// Importer le fichier toys-r-us.sql
source toys-r-us.sql;
```

Voila normalement vôtre projet devrait être fonctionnel
pour verifier rendez-vous sur le lien suivant: [http://localhost:8000](http://localhost:8000)