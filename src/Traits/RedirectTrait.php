<?php

namespace App\Traits;
use Core\App;
use Laminas\Diactoros\Response\RedirectResponse;

trait RedirectTrait
{
  public static function redirect(string $uri, int $status = 302, array $header = [])
  {
    $response = new RedirectResponse($uri, $status, $header);

    $app = App::getApp();
    $app->getRouter()->getPublisher()->publish($response);
    die;
  }
}