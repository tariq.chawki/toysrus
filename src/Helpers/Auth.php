<?php

namespace App\Helpers;
use App\Entities\User;
use App\Traits\RedirectTrait;
use Core\Session\Session;

final class Auth
{
  use RedirectTrait;
  
  private const AUTH_SALT = 'c56a7523d96942a834b9cdc249bd4e8c7aa9';

  private const AUTH_PEPPER = '8d746680fd4d7cbac57fa9f033115fc52196';

  public static function hash(string $str): string
  {
    $data = static::AUTH_SALT . $str . static::AUTH_PEPPER;

    return hash('sha512', $data);
  }

  public function isAuth(): bool
  {
    return !is_null(Session::get(Session::USER));
  }

  public static function hasRole(int $role): bool 
  {
    $user = Session::get(Session::USER);

    return $user && $user->role === $role;
  }

  public static function isSubscriber(): bool
  {
    return static::hasRole(User::ROLE_SUBSCRIBER);
  }

  public static function isAdmin(): bool
  {
    return static::hasRole(User::ROLE_ADMINISTRATOR);
  }
}