<?php

namespace App\Entities;
use Core\Model;

class Toy extends Model
{
  public string $name;

  public string $description;

  public int $brand_id;

  public float $price;
  
  public string $image;

  public string $slug;

  public ?Brand $brand;
}