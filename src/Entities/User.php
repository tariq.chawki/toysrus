<?php

namespace App\Entities;
use Core\Model;

class User extends Model
{
  public const ROLE_SUBSCRIBER = 1;
  
  public const ROLE_ADMINISTRATOR = 2;
  
  public string $email;

  public string $password;

  public int $role;
}