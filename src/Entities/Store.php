<?php

namespace App\Entities;
use Core\Model;

class Store extends Model
{
  public string $name;

  public int $postal_code;

  public string $city;
}