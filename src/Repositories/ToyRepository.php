<?php

namespace App\Repositories;

use App\Entities\Brand;
use App\Entities\Toy;
use Core\Repository;
use PDO;

class ToyRepository extends Repository
{
  public function getTableName(): string
  {
    return 'toys';
  }

  public function findAll(): array
  {
    return $this->readAll(Toy::class);
  }

  public function findBySlug(string $slug): ?Toy
  {
    return $this->readBySlug(Toy::class, $slug);
  }

  public function insert(array $data): bool
  {
    $query = sprintf('INSERT INTO %s (name, slug, description, price, brand_id, image) VALUES (:name, :slug, :description, :price, :brand_id, :image)', $this->getTableName());

    $stmt = $this->pdo->prepare($query);

    return $stmt->execute($data);
  }

  public function findBySlugWithBrand(string $slug): ?Toy
  {
    $query = sprintf('SELECT `%1$s`.* , `%2$s`.name as brand_name
       FROM `%1$s` 
       INNER JOIN `%2$s` ON `%2$s`.id = `%1$s`.brand_id
       WHERE `%1$s`.slug=:slug',
      $this->getTableName(),
      AppRepositoryManager::getRm()->getBrandRepository()->getTableName()
    );

    $stmt = $this->pdo->prepare($query);

    if ($stmt === false) {
      return null;
    }

    $stmt->execute(['slug' => $slug]);

    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($data === false) {
      return null;
    }

    $toy = new Toy($data);
    $toy->brand = new Brand($data);

    return $toy;
  }

  public function findAllByBrand(int $id): ?array
  {
    $query = sprintf('SELECT `%1$s`.* 
       FROM `%1$s` 
       INNER JOIN `%2$s` ON `%2$s`.id = `%1$s`.brand_id
       WHERE `%2$s`.id = :id',
      $this->getTableName(),
      AppRepositoryManager::getRm()->getBrandRepository()->getTableName()
    );

    $stmt = $this->pdo->prepare($query);

    if ($stmt === false) {
      return null;
    }

    $stmt->execute(['id' => $id]);

    $datas = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($datas === false) {
      return null;
    }

    $toys = [];

    foreach ($datas as $data) {
      $toys[] = new Toy($data);
    }

    return $toys;
  }
}