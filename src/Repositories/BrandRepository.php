<?php

namespace App\Repositories;
use App\Entities\Brand;
use Core\Repository;
use PDO;

class BrandRepository extends Repository
{
	public function getTableName(): string 
  {
    return 'brands';
	}
  
  public function findAll(): ?array
  {
    return $this->readAll(Brand::class);
  }

  public function getBrandByName(): ?array
  {
    $query = sprintf('SELECT `%1$s`.id, `%1$s`.name , COUNT(`%2$s`.id) AS total
       FROM `%1$s`
       INNER JOIN `%2$s` ON `%1$s`.id = `%2$s`.brand_id
       GROUP BY `%1$s`.id',
      $this->getTableName(),
      AppRepositoryManager::getRm()->getToyRepository()->getTableName(),
    );

    $stmt = $this->pdo->query($query);

    if ($stmt === false) {
      return null;
    }

    $brands = $stmt->fetchAll(PDO::FETCH_CLASS, Brand::class);

    if ($brands === false)
      return null;

    return $brands;
  }
}