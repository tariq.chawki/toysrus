<?php

namespace App\Repositories;

use App\Traits\RepositoryManagerTrait;
use Core\App;

class AppRepositoryManager
{
  use RepositoryManagerTrait;

  private UserRepository $userRepository;

  private ToyRepository $toyRepository;

  private BrandRepository $brandRepository;

  protected function __construct()
  {
    $config = App::getApp();

    $this->userRepository = new UserRepository($config);
    $this->toyRepository = new ToyRepository($config);
    $this->brandRepository = new BrandRepository($config);
  }

  public function getUserRepository(): UserRepository
  {
    return $this->userRepository;
  }
  
  public function getToyRepository(): ToyRepository
  {
    return $this->toyRepository;
  }

  public function getBrandRepository(): BrandRepository
  {
    return $this->brandRepository;
  }
}