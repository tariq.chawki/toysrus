<?php

namespace App\Repositories;

use App\Entities\User;
use App\Interfaces\DatabaseConfigInterface;
use Core\Repository;

class UserRepository extends Repository
{
  public function __construct(DatabaseConfigInterface $config)
  {
    parent::__construct($config);
  }

  public function getTableName(): string
  {
    return 'users';
  }

  public function findAll(): ?array
  {
    return $this->readAll(User::class);
  }

  public function findById(int $id): ?User
  {
    return $this->readById(User::class, $id);
  }

  public function update(string $email, int $role, int $id): ?User
  {
    $query = sprintf('UPDATE %s SET email=:email, role=:role WHERE id=:id', $this->getTableName());

    $stmt = $this->pdo->prepare($query);

    if ($stmt === false)
      return null;

    $stmt->execute([
      'id' => $id,
      'email' => $email,
      'role' => $role,
    ]);

    $user = $stmt->fetch();

    if ($user === false)
      return null;

    return new User($user);
  }

  public function create(string $email, string $password, int $role): ?User
  {
    $query = sprintf('INSERT INTO %s (email, password, role) VALUES (:email, :password, :role)', $this->getTableName());
    
    $stmt = $this->pdo->prepare($query);

    if ($stmt === false)
    return null;

    $stmt->execute([
      'email' => $email,
      'password' => $password,
      'role' => $role
    ]);

    return $this->checkAuth($email, $password);
  }

  public function delete(int $id): bool
  {
    $query = sprintf('DELETE FROM %s WHERE id = :id', $this->getTableName());
    
    $stmt = $this->pdo->prepare($query);

    return $stmt->execute([
      'id' => $id,
    ]);
  }

  public function checkAuth(string $email, string $password): ?User
  {
    $query = sprintf('SELECT * FROM `%s` WHERE `email`=:email AND `password`=:password', $this->getTableName());

    $stmt = $this->pdo->prepare($query);

    if ($stmt === false)
    return null;
    
    $stmt->execute([
      'email' => $email,
      'password' => $password
    ]);
    
    $user = $stmt->fetchObject(User::class);
    
    if (empty($user))
      return null;

    return $user;
  }
}