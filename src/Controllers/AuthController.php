<?php

namespace App\Controllers;

use App\Entities\User;
use App\Helpers\Auth;
use App\Repositories\AppRepositoryManager;
use Core\Controller;
use Core\Form\FormError;
use Core\Form\FormResult;
use Core\Session\Session;
use Core\View;
use Laminas\Diactoros\ServerRequest;

class AuthController extends Controller
{
  public function index(): void
  {
    $view = new View('auth/login');

    $view->titre = 'Page de Connexion';

    $view->render([
      'form_result' => Session::get(Session::FORM_RESULT)
    ]);
  }

  public function login(ServerRequest $request): void
  {
    $post_data = $request->getParsedBody();
    $form_result = new FormResult();
    $user = new User();

    if (empty($post_data['email'])) {
      $form_result->addError(new FormError('Email', 'Veuillez remplir le champ email'));
    }

    if (empty($post_data['password'])) {
      $form_result->addError(new FormError('Password', 'Veuillez remplir le champ password'));
    }

    $email = $post_data['email'];
    $password = Auth::hash($post_data['password']);

    $user = AppRepositoryManager::getRm()->getUserRepository()->checkAuth($email, $password);

    if (is_null($user)) {
      $form_result->addError(new FormError('', 'Email et/ou mot de passe invalide'));
    }

    if ($form_result->hasError()) {
      Session::set(Session::FORM_RESULT, $form_result);
      self::redirect('/connexion');
    }

    $user->password = '';
    Session::set(Session::USER, $user);

    self::redirect('/');
  }

  public function logout(): void
  {
    Session::destroy();

    static::redirect('/');
  }
}