<?php

namespace App\Controllers;
use App\Helpers\Auth;
use App\Repositories\AppRepositoryManager;
use Core\Controller;
use Core\Session\Session;
use Core\View;

class AdminController extends Controller
{
  public function index(): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $view = new View('admin/index', true);

    $view->titre = 'Page administration';

    $view->render();
  }
}