<?php

namespace App\Controllers;

use App\Repositories\AppRepositoryManager;
use Core\Controller;
use Core\Form\FormError;
use Core\Form\FormResult;
use Core\Session\Session;
use Core\View;
use Laminas\Diactoros\ServerRequest;

class ToyController extends Controller
{
  public function index(): void
  {
    $view = new View('toy/list', true);

    $view->titre = 'Tous les toys';

    $view->render([
      'title_tag' => 'Mon site',
      'h1_tag' => 'Nos jouets',
      'toys' => AppRepositoryManager::getRm()->getToyRepository()->findAll()
    ]);
  }

  public function show(string $slug): void
  {
    $view = new View('toy/detail', true);

    $view->titre = 'Tous les toys';

    $toy = AppRepositoryManager::getRm()->getToyRepository()->findBySlugWithBrand($slug);

    $view->render([
      'title_tag' => $toy->name,
      'toy' => $toy
    ]);
  }

  public function create(): void
  {
    $view = new View('toy/create', true);

    $view->titre = 'Ajouter un nouveau toy';

    $view->render([
      'brands' => AppRepositoryManager::getRm()->getBrandRepository()->findAll()
    ]);
  }

  public function store(ServerRequest $request): void
  {
    $imageData = $_FILES['image'];
    $formData = $request->getParsedBody();
    $formResult = new FormResult();

    $imageType = explode('/', $imageData['type'])[1];
    $types = ['jpeg', 'jpg', 'png'];

    if (!in_array($imageType, $types)) {
      $formResult->addError(new FormError('Image', 'Le format est inconforme'));
    }

    if (
      empty($formData['name']) ||
      empty($formData['slug']) ||
      empty($formData['description']) ||
      empty($formData['price']) ||
      empty($formData['brand_id'])
    ) {
      $formResult->addError(new FormError('', 'Tous les champs sont obligatoire!'));
    }

    $from = $imageData['tmp_name'];
    $imageName = time() . '_' . $imageData['name'];
    $to = APP_IMG_PATH . DIRECTORY_SEPARATOR . $imageName;

    $isUploaded = move_uploaded_file($from, $to);

    if (!$isUploaded) {
      $formResult->addError(new FormError('Image', 'Na pas pu être télécharger!'));
    }

    if ($formResult->hasError()) {
      dd($formResult->getFormError());
      Session::set(Session::FORM_RESULT, $formResult);
      self::redirect('/admin/jouets/create');
    }

    AppRepositoryManager::getRm()->getToyRepository()->insert([
      'name' => $formData['name'],
      'slug' => $formData['slug'],
      'description' => $formData['description'],
      'price' => $formData['price'],
      'brand_id' => $formData['brand_id'],
      'image' => $imageName,
    ]);

    Session::delete(Session::FORM_RESULT);
    self::redirect('/admin');
  }

  public function indexBrand(int $id): void
  {
    $view = new View('toy/list', true);

    $view->titre = 'Tous les toys';

    $toys = AppRepositoryManager::getRm()->getToyRepository()->findAllByBrand($id);

    $view->render([
      'title_tag' => 'Mon site',
      'h1_tag' => 'Nos jouets par marque',
      'toys' => $toys
    ]);
  }
}