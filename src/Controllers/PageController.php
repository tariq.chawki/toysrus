<?php

namespace App\Controllers;
use Core\Controller;
use Core\View;


class PageController extends Controller
{
  public function index(): void
  {
    $view = new View('page/home', true);

    $view->titre = "Mon super titre";

    $view->render([
      'title_tag' => 'mon site',
      'list_title' => 'Bienvenue',
      'toy_list' => [
        'jouet 1',
        'jouet 2'
      ],
    ]);
  }
}