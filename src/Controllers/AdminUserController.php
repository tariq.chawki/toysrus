<?php

namespace App\Controllers;

use App\Helpers\Auth;
use App\Repositories\AppRepositoryManager;
use Core\Controller;
use Core\Form\FormError;
use Core\Form\FormResult;
use Core\Session\Session;
use Core\View;
use Laminas\Diactoros\ServerRequest;

class AdminUserController extends Controller
{
  public function index(): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $view = new View('user/list', true);

    $view->titre = 'Gérée tous les utilisateurs';

    $view->render([
      'title_tag' => 'Dashboard',
      'users' => AppRepositoryManager::getRm()->getUserRepository()->findAll()
    ]);
  }

  public function edit(int $id): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $view = new View('user/edit', true);

    $user = AppRepositoryManager::getRm()->getUserRepository()->findById($id);

    $view->titre = "Modifier l'utilisateur";

    $view->render([
      'form_result' => Session::get(Session::FORM_RESULT),
      'user' => $user
    ]);
  }

  public function update(ServerRequest $request, int $id): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $post_data = $request->getParsedBody();
    $form_result = new FormResult();

    if (empty($post_data['email'])) {
      $form_result->addError(new FormError('Email', 'Veuillez remplir le champ email'));
    }

    if (empty($post_data['role'])) {
      $form_result->addError(new FormError('Role', 'Veuillez selection un role'));
    }

    $user = AppRepositoryManager::getRm()->getUserRepository()->update(
      $post_data['email'],
      $post_data['role'],
      $id
    );

    if (is_null($user)) {
      $form_result->addError(new FormError('', 'Email et/ou mot de passe invalide'));
    }

    if ($form_result->hasError()) {
      Session::set(Session::FORM_RESULT, $form_result);

      self::redirect('/admin/users/' . $id . '/edit');
    }

    $user->password = '';
    Session::set(Session::USER, $user);

    self::redirect('/admin');
  }

  public function create(): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $view = new View('user/create', true);

    $view->titre = "Créer un nouveau utilisateur ";

    $view->render([
      'form_result' => Session::get(Session::FORM_RESULT)
    ]);
  }

  public function store(ServerRequest $request): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $post_data = $request->getParsedBody();
    $form_result = new FormResult();

    if (empty($post_data['email'])) {
      $form_result->addError(new FormError('Email', 'Ce champ est obligatoire'));
    }

    if (empty($post_data['password'])) {
      $form_result->addError(new FormError('Mot de passe', 'Ce champ est obligatoire'));
    }

    if (empty($post_data['role'])) {
      $form_result->addError(new FormError('Role', 'Ce champ est obligatoire'));
    }

    $post_data['password'] = Auth::hash($post_data['password']);

    $user = AppRepositoryManager::getRm()->getUserRepository()->checkAuth($post_data['email'], $post_data['password']);

    if (!is_null($user)) {
      $form_result->addError(new FormError('', 'Cette utilisateur existe déja!'));
    }

    if (!$form_result->hasError()) {
      $user = AppRepositoryManager::getRm()->getUserRepository()->create(
        $post_data['email'],
        $post_data['password'],
        $post_data['role']
      );
    }

    if (is_null($user)) {
      $form_result->addError(new FormError('', 'L\'utilisateur n\'a pas pu être créer!'));
    }

    if ($form_result->hasError()) {
      Session::set(Session::FORM_RESULT, $form_result);

      self::redirect('/admin/users/create');
    }

    self::redirect('/admin');
  }

  public function delete(int $id): void
  {
    if (!Auth::isAdmin()) {
      $this::redirect('/');
    }

    $form_result = new FormResult();

    $isDeleted = AppRepositoryManager::getRm()->getUserRepository()->delete($id);

    if (!$isDeleted) {
      $form_result->addError(new FormError('', 'L\'utilisateur n\'a pas pu être supprimer'));
      Session::set(Session::FORM_RESULT, $form_result);
    }

    $currentUser = Session::get(Session::USER);

    if ($currentUser->id === $id) {
      Session::delete(Session::USER);
    }

    self::redirect('/admin');
  }
}