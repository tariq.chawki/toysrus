<main>
  <h1>
    <?= $this->titre ?>
  </h1>

  <h2><?= $list_title ?></h2>

  <ul>
    <?php foreach ($toy_list as $toy): ?>
      <li><?= $toy ?></li>
    <?php endforeach ?>
  </ul>
</main>