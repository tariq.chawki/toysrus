<div class="col-4 d-flex flex-column border p-3">

    <h1><?= $this->titre ?></h1>

    <?php if (!is_null($form_result) && $form_result->hasError()): ?>
      <div class="alert alert-danger">
        <ul>
          <?php foreach ($form_result->getFormError() as $error): ?>
            <li>
              <?php if ($error->getErrorName()): ?>
                <?= $error->getErrorName() ?>:
              <?php endif ?>
              <?= $error->getErrorMessage() ?>
            </li>
          <?php endforeach ?>
        </ul>
      </div>
    <?php endif ?>
    
    <form action="/admin/users/<?= $user->id ?>/update" method="post">
        <div class="mb-3">
            <!-- <input type="hidden" value="<?= $user->id ?>" name="id" class="form-control" id="email"
                   aria-describedby="emailHelp">
            <input type="hidden" value="<?= $user->password ?>" name="password" class="form-control" id="password"> -->
            <label for="email" class="form-label">Email</label>
            <input type="text" value="<?= $user->email ?>" name="email" class="form-control" id="email"
                   aria-describedby="emailHelp">
        </div>
        <div class=" d-flex flex-column mb-3">
            <div>
                <label for="role" class="form-label">Son rôle:</label>
            </div>
            <div>
                <input type="radio" name="role" value="1" <?=  $user->role == 1 ? 'checked' : ''?>>
                <label for="role">Utilisateur</label>
            </div>
            <div>
                <input type="radio" name="role" value="2" <?=  $user->role == 2 ? 'checked' : ''?>>
                <label for="role">Administrateur</label>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Enregistrer</button>
    </form>
</div>

