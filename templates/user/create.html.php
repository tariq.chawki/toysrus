<div class="col-4 d-flex flex-column border p-3">

  <h1>
    <?= $this->titre ?>
  </h1>

  <?php if (!is_null($form_result) && $form_result->hasError()): ?>
      <div class="alert alert-danger">
        <ul>
          <?php foreach ($form_result->getFormError() as $error): ?>
            <li>
              <?php if ($error->getErrorName()): ?>
                <?= $error->getErrorName() ?>:
              <?php endif ?>
              <?= $error->getErrorMessage() ?>
            </li>
          <?php endforeach ?>
        </ul>
      </div>
    <?php endif ?>

  <form action="/admin/users" method="post">

    <div class="form-group">
      <label for="email" class="form-label">Email</label>
      <input type="text" name="email" class="form-control" id="email" aria-describedby="emailHelp">
    </div>

    <div class="form-group mb-3">
      <label for="password" class="form-label">Mot de passe</label>
      <input type="password" name="password" class="form-control" id="password">
    </div>

    <div class=" d-flex flex-column mb-3">
      <div>
        <label for="role" class="form-label">Son rôle:</label>
      </div>
      <div>
        <input type="radio" name="role" value="1">
        <label for="role">Utilisateur</label>
      </div>
      <div>
        <input type="radio" name="role" value="2">
        <label for="role">Administrateur</label>
      </div>
    </div>

    <button type="submit" class="btn btn-success">Enregistrer</button>

  </form>
</div>