<h1>
  <?= $this->titre ?>
</h1>

<?php if (empty($users)): ?>

  <p>Aucun utilisateur enregistré</p>

<?php else: ?>

  <a href="/admin/users/create" class="btn btn-primary">Ajouter un utilisateur</a>

  <div class="d-flex flex-row flex-wrap justify-content-center col-6">

    <?php foreach ($users as $user): ?>
      <div class="card w-75 m-2">
        <div class="card-body">

          <h5 class="card-title"><?= $user->email ?></h5>
          <p class="card-text">
            <?= $user->role === 1 ? 'Utilisateur' : 'Administrateur' ?>
          </p>

          <a href="/admin/users/<?= $user->id ?>/edit" class="btn btn-success">Modifier</a>
          <a href="/admin/users/<?= $user->id ?>" class="btn btn-danger">Supprimer</a>

        </div>
      </div>
    <?php endforeach ?>

  </div>

<?php endif ?>