<?php

if ($auth->isAuth()) {
  $auth::redirect('/');
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body class="d-flex justify-content-center">

  <div class="col-4 d-flex flex-column border p-3">

    <h1 class="my-4">
      <?= $this->titre ?>
    </h1>

    <?php if (!is_null($form_result) && $form_result->hasError()): ?>
      <div class="alert alert-danger">
        <ul>
          <?php foreach ($form_result->getFormError() as $error): ?>
            <li>
              <?php if ($error->getErrorName()): ?>
                <?= $error->getErrorName() ?>:
              <?php endif ?>
              <?= $error->getErrorMessage() ?>
            </li>
          <?php endforeach ?>
        </ul>
      </div>
    <?php endif ?>

    <form action="/login" method="POST" novalidate>
      <div class="form-group mb-2">
        <label for="email" class="form-label">Email</label>
        <input type="email" name="email" id="email" class="form-control">
      </div>

      <div class="form-group mb-2">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" name="password" id="password" class="form-control">
      </div>

      <button class="btn btn-primary" type="submit">Se connecter</button>
    </form>
  </div>

</body>

</html>