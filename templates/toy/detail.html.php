<div class="d-flex flex-row flex-wrap justify-content-center col-6 m-3" style="max-with: 1000px">

  <h1><?= $title_tag ?></h1>

  <div class="d-flex mt-3 gap-4">

    <div class="col-4">
      <img src="/img/<?= $toy->image ?>" class="card-img-top" alt="<?= $toy->name ?>">
      <h3 class="text-succes mt-3"><?= $toy->brand->name ?></h3>
    </div>

    <div class="col-8">
      <p>
        <?= $toy->description ?>
      </p>
      
    </div>

  </div>

</div>