<div class="d-flex flex-wrap justify-content-center gap-5" style="max-width: 900px">
  <?php foreach ($toys as $toy): ?>
    <div class="card" style="width: 18rem;">
      <img src="/img/<?= $toy->image ?>" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">
          <?= $toy->name ?>
        </h5>
        <p class="h4">
          <?= $toy->price ?>€
        </p>
        <a href="/jouets/<?= $toy->slug ?>" class="btn btn-primary">Acheter</a>
      </div>
    </div>
  <?php endforeach ?>
</div>