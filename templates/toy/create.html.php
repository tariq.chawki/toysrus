<form action="/admin/jouets" method="POST" enctype="multipart/form-data">

  <div class="form-group mb-2">
    <label for="name" class="form-label">Nom</label>
    <input class="form-control" type="text" name="name" value="" />
  </div>

  <div class="form-group mb-2">
    <label for="slug" class="form-label">Slug</label>
    <input class="form-control" type="text" name="slug" value="" />
  </div>

  <div class="form-group mb-2">
    <label for="description" class="form-label">Description</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
  </div>

  <div class="form-group mb-2">
    <label for="brand_id" class="form-label">Marque</label>
    <select class="form-select" name="brand_id" id="brand_id">
      <?php foreach ($brands as $brand): ?>
        <option value="<?= $brand->id ?>">
          <?= $brand->name ?>
        </option>
      <?php endforeach ?>
    </select>
  </div>

  <div class="form-group mb-2">
    <label for="price" class="form-label">Prix</label>
    <input class="form-control" type="number" name="price" value="" />
  </div>

  <div class="form-group mb-4">
    <label for="image" class="form-label">Image</label>
    <input class="form-control" type="file" name="image" id="image"/>
  </div>

  <button class="btn btn-primary" type="submit">Ajouter le fichier</button>
</form>