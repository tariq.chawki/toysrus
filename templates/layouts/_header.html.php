<?php

$uri = $_SERVER['REQUEST_URI'];

$routes = explode('/', $uri);

$currentUrl = '/';

$links = [];

foreach ($routes as $key => $value) {
  $prefix = strlen($currentUrl) > 1 ? '/' : '';
  $currentUrl .= $prefix . $value;
  $links[] = sprintf('<a href="%s">%s</a>', $currentUrl, $value);
}

use App\Repositories\AppRepositoryManager;

if (!$auth->isAuth()) {
  $auth::redirect('/connexion');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    <?= $this->titre ?>
  </title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  <link rel="stylesheet" href="/style.css">
</head>

<body>

  <div id="container">

    <header>
      <div class="logo"><a href="/"><img src="/img/logo.jpg"></div>
    </header>

    <div id="top-bar" class="d-flex flex-row justify-content-center mb-4">
      <div id="main-menu">
        <ul class="menu-root d-flex flex-row justify-content-center">

          <?php if ($auth::isAdmin()): ?>
            <li>
              <a href="/admin">Accès admin</a>
              <ul class="menu-root">
                <li>
                  <a href="/admin/users">Géstion des utilisateurse</a>
                </li>
                <li>
                  <a href="/admin/toys">Géstion des utilisateurse</a>
                </li>
              </ul>
            </li>
          <?php endif ?>

          <li>
            <a href="/">Accueil</a>
          </li>
          <li>
            <a href="/jouets">Tous les jouets</a>
          </li>
          <li>
            <a href="#">Par marque
              <i class="bi bi-chevron-down"></i>
            </a>
            <ul>
              <?php foreach (AppRepositoryManager::getRm()->getBrandRepository()->getBrandByName() as $brand): ?>
                <li>
                  <a href='/brands/<?= $brand->id ?>'>
                    <?= $brand->name ?> (<?= $brand->total ?>)
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </li>
        </ul>
      </div>

      <div class="d-flex align-items-center m-2">
        <li>
          <a href="/deconnexion" class="logout"><i class="bi bi-box-arrow-left"></i></a>
        </li>
      </div>
    </div>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <?php foreach ($links as $link): ?>
          <li class="breadcrumb-item" aria-current="page">
            <?= $link ?>
          </li>
        <?php endforeach ?>
      </ol>
    </nav>