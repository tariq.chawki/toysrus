<?php

namespace Core;

use App\Interfaces\DatabaseConfigInterface;
use PDO;

abstract class Repository
{
  protected PDO $pdo;

  public function __construct(DatabaseConfigInterface $config)
  {
    $this->pdo = Database::getPDO($config);
  }

  abstract public function getTableName(): string;

  protected function readAll(string $class): ?array
  {
    $query = sprintf('SELECT * FROM `%s`', $this->getTableName());

    $stmt = $this->pdo->query($query);

    $all = $stmt->fetchAll(PDO::FETCH_CLASS, $class);

    if ($all === false)
      return null;

    return $all;
  }

  protected function readById(string $class, int $id): ?object
  {
    return $this->readByColumn($class, 'id', $id);
  }

  protected function readBySlug(string $class, string $slug): ?object
  {
    return $this->readByColumn($class, 'slug', $slug);
  }

  private function readByColumn(string $class, string $column, mixed $value): ?object
  {
    $query = sprintf('SELECT * FROM `%s` WHERE', $this->getTableName());

    if ($column === 'id') {
      $query .= ' `id`=:value';
    } else {
      $query .= ' `slug`=:value';
    }

    $stmt = $this->pdo->prepare($query);

    if ($stmt === false) {
      return null;
    }
    
    $stmt->execute(['value' => $value]);
    
    $row = $stmt->fetchObject($class);

    if ($row === false) {
      return null;
    }

    return $row;
  }
}