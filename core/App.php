<?php

namespace Core;

use App\Controllers\AdminController;
use App\Controllers\AdminUserController;
use App\Controllers\AuthController;
use App\Controllers\PageController;
use App\Controllers\ToyController;
use App\Interfaces\DatabaseConfigInterface;
use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Router;

class App implements DatabaseConfigInterface
{
  private const DB_HOST = 'db';
  
  private const DB_NAME = 'toysrus';

  private const DB_USER = 'root';

  private const DB_PASS = 'root';

  private static ?self $instance = null;

  private Router $router;

  private function __construct()
  {
    $this->router = Router::create();
  }

  public function start()
  {
    $this->registerRoutes();
    $this->startRouter();
  }

  public function registerRoutes()
  {
    $this->router->pattern('id', '[1-9]\d*');
    $this->router->pattern('slug', '(\d+-)?[a-z]+(-[a-z-\d]+)*');
    $this->router->get('/', [PageController::class, 'index']);

    $this->router->get('/admin', [AdminController::class, 'index']);

    // USERS
    $this->router->get('/admin/users/{id}/edit', [AdminUserController::class, 'edit']);
    $this->router->post('/admin/users/{id}/update', [AdminUserController::class, 'update']);
    $this->router->get('/admin/users/create', [AdminUserController::class, 'create']);
    $this->router->get('/admin/users/{id}', [AdminUserController::class, 'delete']);
    $this->router->post('/admin/users', [AdminUserController::class, 'store']);
    $this->router->get('/admin/users', [AdminUserController::class, 'index']);

    // JOUETS
    $this->router->get('/jouets', [ToyController::class, 'index']);
    $this->router->get('/jouets/{slug}', [ToyController::class, 'show']);
    $this->router->get('/admin/jouets/create', [ToyController::class, 'create']);
    $this->router->post('/admin/jouets', [ToyController::class, 'store']);
    
    // BRANDS
    $this->router->get('/brands/{id}', [ToyController::class, 'indexBrand']);

    // AUTH
    $this->router->get('/connexion', [AuthController::class, 'index']);
    $this->router->post('/login', [AuthController::class, 'login']);
    $this->router->get('/deconnexion', [AuthController::class, 'logout']);
  }

  public function startRouter()
  {
    try {
      $this->router->dispatch();
    } catch (RouteNotFoundException $e) {
      echo $e;
    } catch (InvalidCallableException $e) {
      echo $e;
    }
  }

  public static function getApp(): self
  {
    if (!isset(static::$instance))
    {
      static::$instance = new static();
    }

    return static::$instance;
  }

  public function getRouter(): Router
  {
    return $this->router;
  }

  /**
   * @return string
   */
  public function getHost(): string
  {
    return static::DB_HOST;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return static::DB_NAME;
  }

  /**
   * @return string
   */
  public function getUser(): string
  {
    return static::DB_USER;
  }

  /**
   * @return string
   */
  public function getPass(): string
  {
    return static::DB_PASS;
  }
}