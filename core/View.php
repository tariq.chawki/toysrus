<?php

namespace Core;

use App\Helpers\Auth;

class View
{
  public const PATH_VIEW = APP_BASE_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;

  public const PATH_PARTIALS = self::PATH_VIEW . 'layouts' . DIRECTORY_SEPARATOR;

  public string $titre = 'Titre par défaut';

  public function __construct(
    private string $name,
    private bool $is_complete = false
  )
  {
  }

  private function getRequirePath(): string
  {
    $arr_name = explode('/', $this->name);

    $category = $arr_name[0];
    $name = $arr_name[1];

    $name_prefix = $this->is_complete ? '' : '_';

    return self::PATH_VIEW . $category . DIRECTORY_SEPARATOR . $name_prefix . $name . '.html.php';
  }

  public function render(?array $view_data = null): void
  {
    $auth = new Auth();

    if (isset($view_data)) {
      extract($view_data);
    }
    
    ob_start();
    
    if ($this->is_complete) {
      require self::PATH_PARTIALS . '_header.html.php';
    }

    require_once $this->getRequirePath();

    if ($this->is_complete) {
      require self::PATH_PARTIALS . '_footer.html.php';
    }

    ob_end_flush();
  }
}