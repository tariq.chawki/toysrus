<?php

namespace Core;

use App\Interfaces\ControllerInterface;
use App\Traits\RedirectTrait;
use Laminas\Diactoros\Response\RedirectResponse;

abstract class Controller implements ControllerInterface
{
  use RedirectTrait;
}