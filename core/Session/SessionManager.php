<?php

namespace Core\Session;

abstract class SessionManager
{
  public static function isSessionStart(): bool
  {
    return session_status() === PHP_SESSION_ACTIVE;
  }

  public static function startSession(): void
  {
    if (!static::isSessionStart()) {
      session_start();
    }
  }

  public static function set(string $key, mixed $value): void
  {
    static::startSession();

    $_SESSION[$key] = $value;
  }

  public static function get(string $key): mixed
  {
    static::startSession();

    return $_SESSION[$key] ?? null;
  }

  public static function delete(string $key): void
  {
    static::startSession();

    if (static::get($key)) {
      unset($_SESSION[$key]);
    }
  }

  public static function destroy(): void
  {
    static::startSession();

    if (static::isSessionStart()) {
      session_destroy();
    }
  }
}