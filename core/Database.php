<?php

namespace Core;

use App\Interfaces\DatabaseConfigInterface;
use PDO;

final class Database
{
  private const OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
  ];

  private static ?PDO $pdo = null;

  private function __construct()
  {
  }

  private function __clone()
  {
  }

  public static function getPDO(DatabaseConfigInterface $config): PDO
  {
    $dns = sprintf(
      "mysql:dbname=%s;host=%s",
      $config->getName(),
      $config->getHost()
    );

    if (!isset(static::$pdo)) {
      static::$pdo = new PDO(
        $dns,
        $config->getUser(),
        $config->getPass(),
          static::OPTIONS
      );
    }

    return static::$pdo;
  }
}