<?php

namespace Core\Form;
use Core\Session\Session;

class FormResult
{
  private string $success_message;

  private array $form_error = [];

  public function __construct(string $success_message = '')
  {
    $this->success_message = $success_message;
  }

  public function getFormError(): array
  {
    return $this->form_error;
  }

  public function getSuccessMessage(): string
  {
    return $this->success_message;
  }

  public function addError(FormError $error): void
  {
    $this->form_error[] = $error;
  }

  public function hasError(): bool
  {
   if (empty($this->form_error)) {
      Session::delete(Session::FORM_RESULT);
      return false;
   }

    return true;
  }
}