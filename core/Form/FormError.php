<?php

namespace Core\Form;

class FormError
{
  public function __construct(
    private string $error_name = '',
    private string $error_message = ''
  )
  {
  }

  public function getErrorName(): string
  {
    return $this->error_name;
  }
  
  public function getErrorMessage(): string
  {
    return $this->error_message;
  }
}